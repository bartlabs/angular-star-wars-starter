import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ThemesEnum as themes } from '@app/enums';
import { ThemesService } from '@app/services';

@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.scss']
})
export class ModulesComponent implements OnInit {
  ifDarkSide: boolean;
  ifLightSide: boolean;

  constructor(
    private _router: Router,
    private _themesService: ThemesService
  ) {
    this._resetSide();
  }

  ngOnInit(): void {
  }

  onDarkSideClick() {
    this._themesService.setTheme(themes.Dark);
    this._router.navigate(['/dashboard']);
  }

  onLightSideClick() {
    this._themesService.setTheme(themes.Light);
    this._router.navigate(['/dashboard']);
  }

  overDark() {
    this.ifDarkSide = true;
  }

  overLight() {
    this.ifLightSide = true;
  }

  out() {
    this._resetSide();
  }

  private _resetSide() {
    this.ifDarkSide = false;
    this.ifLightSide = false;
  }
}
