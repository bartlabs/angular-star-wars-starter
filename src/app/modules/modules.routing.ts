import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ModulesComponent } from './modules.component';

export const routes: Routes = [
    {
        path: '',
        component: ModulesComponent,
        data: { breadcrumb: 'dashboard' },
    }
];

export const ModulesRouting: ModuleWithProviders = RouterModule.forChild(routes);
