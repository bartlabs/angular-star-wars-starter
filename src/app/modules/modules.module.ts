import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DashboardModule } from './dashboard';
import { ModulesComponent } from './modules.component';
import { ModulesRouting } from './modules.routing';

@NgModule({
  declarations: [ModulesComponent],
  imports: [
    CommonModule,
    DashboardModule,
    ModulesRouting
  ]
})
export class ModulesModule { }
