import { DialogModule } from 'primeng/dialog';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ComponentsModule } from '@app/components/components.module';

import {
  FilmsComponent, PeopleComponent, PlanetsComponent, SpeciesComponent,
  StarshipsComponent, VehiclesComponent
} from './components';
import { DashboardComponent } from './dashboard.component';
import { DashboardRouting } from './dashboard.routing';

@NgModule({
  declarations: [
    DashboardComponent,
    FilmsComponent,
    PeopleComponent,
    PlanetsComponent,
    SpeciesComponent,
    StarshipsComponent,
    VehiclesComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    ComponentsModule,
    DashboardRouting,
    DialogModule,
    FormsModule
  ]
})
export class DashboardModule { }
