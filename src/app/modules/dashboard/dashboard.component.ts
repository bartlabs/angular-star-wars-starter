import map from 'lodash/map';

import { Component, OnInit } from '@angular/core';
import { ROUTES_PATHS_CONFIG } from '@app/configs';
import { ModulesEnum } from '@app/enums';
import { RoutesPathConfigModel, SidebarItemsModel } from '@app/models';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  sidebarItems: SidebarItemsModel[];

  constructor() { }

  ngOnInit(): void {
    const dashboardPaths: RoutesPathConfigModel = ROUTES_PATHS_CONFIG[ModulesEnum.Dashboard];
    const mainPath = dashboardPaths.Path;
    const children = dashboardPaths.Child;

    mainPath && children &&
      (this.sidebarItems = map(children, child => ({
        RouterLink: [child.Path],
        Title: child.Name,
      })));
  }

}
