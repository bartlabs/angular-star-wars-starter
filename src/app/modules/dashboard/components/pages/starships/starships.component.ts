import { Component, OnInit } from '@angular/core';
import { RoutesService, SwapiStarshipsService } from '@app/services';

import { PagesComponent } from '../pages.component';

@Component({
  selector: 'app-starships',
  templateUrl: '../pages.component.html', // Common template
  styleUrls: ['./starships.component.scss']
})
export class StarshipsComponent extends PagesComponent implements OnInit {

  constructor(
    private _routesService: RoutesService,
    private _swapiService: SwapiStarshipsService
  ) {
    super(
      _routesService,
      _swapiService
    );

    this.columns = [
      { field: 'name', header: 'Name', },
      { field: 'model', header: 'Model' },
      { field: 'manufacturer', header: 'Manufacturer' },
      { field: 'starship_class', header: 'Starship class' }
    ];
  }

  ngOnInit(): void {
    this.getList();
  }
}
