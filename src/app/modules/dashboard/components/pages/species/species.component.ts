import { Component, OnInit } from '@angular/core';
import { RoutesService, SwapiSpeciesService } from '@app/services';

import { PagesComponent } from '../pages.component';

@Component({
  selector: 'app-species',
  templateUrl: '../pages.component.html', // Common template
  styleUrls: ['./species.component.scss']
})
export class SpeciesComponent extends PagesComponent implements OnInit {

  constructor(
    private _routesService: RoutesService,
    private _swapiService: SwapiSpeciesService
  ) {
    super(
      _routesService,
      _swapiService
    );

    this.columns = [
      { field: 'name', header: 'Name', },
      { field: 'classification', header: 'Classification' },
      { field: 'designation', header: 'Designation' },
      { field: 'language', header: 'Language' }
    ];
  }

  ngOnInit(): void {
    this.getList();
  }
}
