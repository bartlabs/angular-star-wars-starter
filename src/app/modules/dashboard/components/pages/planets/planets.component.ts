import { Component, OnInit } from '@angular/core';
import { RoutesService, SwapiPlanetsService } from '@app/services';

import { PagesComponent } from '../pages.component';

@Component({
  selector: 'app-planets',
  templateUrl: '../pages.component.html', // Common template
  styleUrls: ['./planets.component.scss']
})
export class PlanetsComponent extends PagesComponent implements OnInit {

  constructor(
    private _routesService: RoutesService,
    private _swapiService: SwapiPlanetsService
  ) {
    super(
      _routesService,
      _swapiService
    );

    this.columns = [
      { field: 'name', header: 'Name', },
      { field: 'climate', header: 'Climate' },
      { field: 'gravity', header: 'Gravity' },
      { field: 'population', header: 'Population' }
    ];
  }

  ngOnInit(): void {
    this.getList();
  }
}
