import { Component, OnInit } from '@angular/core';
import { RoutesService, SwapiPeopleService } from '@app/services';

import { PagesComponent } from '../pages.component';

@Component({
  selector: 'app-people',
  templateUrl: '../pages.component.html', // Common template
  styleUrls: ['./people.component.scss'],
})
export class PeopleComponent extends PagesComponent implements OnInit {

  constructor(
    private _routesService: RoutesService,
    private _swapiService: SwapiPeopleService
  ) {
    super(
      _routesService,
      _swapiService
    );

    this.columns = [
      { field: 'name', header: 'Name', },
      { field: 'gender', header: 'Gender' },
      { field: 'birth_year', header: 'Birth year' }
    ];
  }

  ngOnInit(): void {
    this.getList();
  }
}
