import { Component, OnInit } from '@angular/core';
import { RoutesService, SwapiVehiclesService } from '@app/services';

import { PagesComponent } from '../pages.component';

@Component({
  selector: 'app-vehicles',
  templateUrl: '../pages.component.html', // Common template
  styleUrls: ['./vehicles.component.scss']
})
export class VehiclesComponent extends PagesComponent implements OnInit {

  constructor(
    private _routesService: RoutesService,
    private _swapiService: SwapiVehiclesService
  ) {
    super(
      _routesService,
      _swapiService
    );

    this.columns = [
      { field: 'name', header: 'Name', },
      { field: 'model', header: 'Model' },
      { field: 'manufacturer', header: 'Manufacturer' },
      { field: 'vehicle_class', header: 'Vehicle class' }
    ];
  }

  ngOnInit(): void {
    this.getList();
  }
}
