export * from './films/films.component';
export * from './pages.component';
export * from './people/people.component';
export * from './planets/planets.component';
export * from './species/species.component';
export * from './starships/starships.component';
export * from './vehicles/vehicles.component';
