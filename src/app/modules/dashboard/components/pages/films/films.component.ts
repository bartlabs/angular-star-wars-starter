import { Component, OnInit } from '@angular/core';
import { RoutesService, SwapiFilmsService } from '@app/services';

import { PagesComponent } from '../pages.component';

@Component({
  selector: 'app-films',
  templateUrl: '../pages.component.html', // Common template
  styleUrls: ['./films.component.scss']
})
export class FilmsComponent extends PagesComponent implements OnInit {

  constructor(
    private _routesService: RoutesService,
    private _swapiService: SwapiFilmsService
  ) {
    super(
      _routesService,
      _swapiService
    );

    this.columns = [
      { field: 'title', header: 'Title' },
      { field: 'director', header: 'Director'},
      { field: 'producer', header: 'Producer' },
      { field: 'release_date', header: 'Release date' }
    ];
  }

  ngOnInit(): void {
    this.getList();
  }
}
