import { cloneDeep, isNil } from 'lodash';

import { Component, OnInit } from '@angular/core';
import { SwapiQueryParamsModel } from '@app/models';
import { SwapiInterface } from '@app/interfaces';
import { RoutesService, SwapiBaseService } from '@app/services';
import { SwapiType } from '@app/types';

@Component({
  selector: 'app-pages',
  template: '',
  styleUrls: ['./pages.component.scss'],
})
export class PagesComponent implements OnInit {
  private _columns: any[];
  private _count: number;
  private _data: SwapiType[];
  private _first: number;
  private _rows: number;

  details: SwapiInterface;
  ifDetailsModalVisible: boolean;
  searchText: string;
  queryParams: SwapiQueryParamsModel;
  getServiceList: any;

  get getColumns() {
    return this._columns;
  }

  get getCount() {
    return this._count;
  }

  get getData() {
    return this._data;
  }

  get getFirst() {
    return this._first;
  }

  get getRows() {
    return this._rows;
  }

  set columns(columns: any[]) {
    this._columns = columns;
  }

  constructor(
    public routesService: RoutesService,
    public swapiService: SwapiBaseService
  ) {
    this._first = 0;
    this._rows = 10;
    this.searchText = null;
    this.queryParams = new SwapiQueryParamsModel();

    const pageParamName = 'page';
    const queryData = this.routesService.getQueryData([pageParamName]);
    if (queryData[pageParamName]) {
      const page = Number(queryData[pageParamName]);
      this.queryParams.page = page;
      this._first = (page * this._rows) - this._rows;
    }
  }

  ngOnInit(): void {
  }

  onPageChange(event) {
    this.queryParams = new SwapiQueryParamsModel();

    !isNil(event.first) && (this._first = event.first);
    event.rows && (this._rows = event.rows);
    const page = Number(this._first / event.rows) + 1;

    if (page && page > 1) {
      this.queryParams.page = page;
      this.routesService.setQueryParams({ page });
    } else {
      this._resetPage();
    }
  }

  onRowClick(details: SwapiInterface) {
    this.details = cloneDeep(details);
    this.ifDetailsModalVisible = true;
  }

  onSearch() {
    this._resetPage();

    this.queryParams = new SwapiQueryParamsModel();
    this.searchText && (this.queryParams.q = this.searchText);
    this.getList();
  }

  getList() {
    return this.swapiService.getList(this.queryParams).subscribe(
      data => {
        data && this._setData(data);
      },
      error => {
        console.log(error); // #TODO - error handler
      }
    );
  }

  private _resetPage() {
    this._first = 0;
    this.queryParams.page && delete this.queryParams.page;
    this.routesService.setQueryParams();
  }

  private _setData(data: SwapiType[]) {
    this._count = data.length || 0;
    this._data = data;
  }
}
