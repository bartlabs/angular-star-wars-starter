import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ROUTES_PATHS_CONFIG as PATHS } from '@app/configs';
import { ModulesEnum } from '@app/enums';

import {
  FilmsComponent, PeopleComponent, PlanetsComponent, SpeciesComponent, StarshipsComponent,
  VehiclesComponent
} from './components';
import { DashboardComponent } from './dashboard.component';

const path = PATHS[ModulesEnum.Dashboard].Path;
const child = PATHS[ModulesEnum.Dashboard].Child;

export const routes: Routes = [
  {
    path,
    component: DashboardComponent,
    data: { breadcrumb: 'dashboard' },
    children: [
      {
        path: child.Films.Path,
        component: FilmsComponent,
        data: { title: 'Films', breadcrumb: 'Films' }
      },
      {
        path: child.People.Path,
        component: PeopleComponent,
        data: { title: 'People', breadcrumb: 'People' }
      },
      {
        path: child.Planets.Path,
        component: PlanetsComponent,
        data: { title: 'Planets', breadcrumb: 'Planets' }
      },
      {
        path: child.Species.Path,
        component: SpeciesComponent,
        data: { title: 'Species', breadcrumb: 'Species' }
      },
      {
        path: child.Starships.Path,
        component: StarshipsComponent,
        data: { title: 'Starships', breadcrumb: 'Starships' }
      },
      {
        path: child.Vehicles.Path,
        component: VehiclesComponent,
        data: { title: 'Vehicles', breadcrumb: 'Vehicles' }
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'films'
      }
    ]
  }
];

export const DashboardRouting: ModuleWithProviders = RouterModule.forChild(routes);
