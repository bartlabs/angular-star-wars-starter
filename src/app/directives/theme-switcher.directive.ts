import { Directive } from '@angular/core';
import { ThemesService } from '@app/services';

@Directive({
  selector: '[appThemeSwitcher]'
})
export class ThemeSwitcherDirective {

  constructor(private _themesService: ThemesService) {
    _themesService.getThemeFromStorage();
  }

  // #TODO - expand...

}
