import {
    GeneralConfigUrlsEnum, ThemesEnum, ThemesPropsEnum, UrlsConfigResourcesEnum
} from '../enums';

export type GeneralConfigUrlsType = {
    [key in GeneralConfigUrlsEnum]: string
};

export type ThemesConfigType = {
    [key in ThemesEnum]: {
        // tslint:disable-next-line: no-shadowed-variable
        [key in ThemesPropsEnum]: string;
    }
};

export type UrlsConfigType = {
    [key in GeneralConfigUrlsEnum]?: {
        // tslint:disable-next-line: no-shadowed-variable
        [key in UrlsConfigResourcesEnum]: string;
    }
};
