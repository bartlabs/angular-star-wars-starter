import { AssetsEnum, AssetsTypeEnum } from '../enums';

export type AssetsConfigType = {
    [key in AssetsTypeEnum]?: {
        // tslint:disable-next-line: no-shadowed-variable
        [key in AssetsEnum]: string;
    }
};
