import {
    SwapiFilmsInterface, SwapiPeopleInterface, SwapiPlanetsInterface, SwapiRootInterface, SwapiSpeciesInterface,
    SwapiStarshipsInterface, SwapiVehiclesInterface
} from '@app/interfaces/swapi';

export type SwapiType = SwapiFilmsInterface | SwapiPeopleInterface | SwapiPlanetsInterface | SwapiRootInterface | SwapiSpeciesInterface
    | SwapiStarshipsInterface | SwapiVehiclesInterface;
