import { SwapiType } from '@app/types';

export interface SwapiInterface {
    count: number;
    data: SwapiType[];
}
