export interface SwapiQueryParamsInterface {
    page: number;
    search: string;
}
