export enum AssetsTypeEnum {
    Icons = 'icons',
    Images = 'images'
}

export enum AssetsEnum {
    DefaultAvatar
}
