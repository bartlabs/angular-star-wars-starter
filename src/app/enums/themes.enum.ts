export enum ThemesEnum {
    Dark = 'Dark',
    Light = 'Light'
}

export enum ThemesPropsEnum {
    ColorVeryDark = '--colorVeryDark',
    ColorDark = '--colorDark',
    ColorMedium = '--colorMedium',
    ColorLight = '--colorLight',
    ColorVeryLight = '--colorVeryLight',
    ColorAction = '--colorAction'
}
