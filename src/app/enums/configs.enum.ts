export enum GeneralConfigUrlsEnum {
    Local,
    SwapiApi
}

export enum UrlsConfigResourcesEnum {
    Films,
    Menu,
    People,
    Planets,
    Species,
    Starship,
    Vehicles
}
