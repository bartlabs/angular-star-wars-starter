import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AppComponent } from '@app/app.component';
import { AppRouting } from '@app/app.routing';

import { environment } from '../environments/environment';
import { ThemeSwitcherDirective } from './directives/theme-switcher.directive';
import { ModulesModule } from './modules';
import { RoutesService } from './services';

@NgModule({
  declarations: [
    AppComponent,
    ThemeSwitcherDirective
  ],
  imports: [
    AppRouting,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    FormsModule,
    HttpClientModule,
    ModulesModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    RoutesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
