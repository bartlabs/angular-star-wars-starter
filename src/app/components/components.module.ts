import { PaginatorModule } from 'primeng/paginator';
import { TableModule } from 'primeng/table';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

import {
    ContentHeaderComponent, DataTableComponent, DetailsCardComponent, NavbarComponent,
    SidebarComponent
} from './';

@NgModule({
  declarations: [
    ContentHeaderComponent,
    DataTableComponent,
    DetailsCardComponent,
    NavbarComponent,
    SidebarComponent
  ],
  exports: [
    ContentHeaderComponent,
    DetailsCardComponent,
    DataTableComponent,
    NavbarComponent,
    SidebarComponent
  ],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    PaginatorModule,
    RouterModule,
    TableModule
  ]
})
export class ComponentsModule { }
