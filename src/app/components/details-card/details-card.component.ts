import { get, head } from 'lodash';

import { Component, Input, OnInit } from '@angular/core';
import { AssetsEnum } from '@app/enums';
import { prepareSwapiData } from '@app/utils';
import { getImagePath } from '@app/utils/assets';

@Component({
  selector: 'app-details-card',
  templateUrl: './details-card.component.html',
  styleUrls: ['./details-card.component.scss']
})
export class DetailsCardComponent implements OnInit {
  private _details: any[];

  title: string;
  imageUrl: string;

  @Input() set details(details: any[]) {
    this.titleField &&
      (this.title = get(details, this.titleField)) &&
      delete details[this.titleField];

    this._details = prepareSwapiData(details);
    this.imageUrl = getImagePath(AssetsEnum.DefaultAvatar);
  }

  get details() {
    return this._details;
  }

  @Input() titleField: string;

  constructor() {
  }

  ngOnInit(): void {
  }

  getValue(values: []): any {
    return (values.length > 1) ? values.length : get(head(values), 'value');
  }

}
