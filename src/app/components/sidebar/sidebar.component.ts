import { Component, Input, OnInit } from '@angular/core';
import { SidebarItemsModel } from '@app/models';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() items: SidebarItemsModel;

  constructor() { }

  ngOnInit(): void {
  }

}
