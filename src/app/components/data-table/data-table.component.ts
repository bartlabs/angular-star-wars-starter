import get from 'lodash/get';

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {
  @Input() columns: string[];
  @Input() externalPaginator: boolean;
  @Input() first: number;
  @Input() internalPaginator: boolean;
  @Input() loading: boolean;
  @Input() noDataSign: string;
  @Input() rowHover: boolean;
  @Input() rows: number;
  @Input() rowsPerPageOptions: number[];
  @Input() separatePaginator: boolean;
  @Input() totalRecords: number;
  @Input() value: any[];

  @Output() pageChange = new EventEmitter<object>();
  @Output() rowClick = new EventEmitter<object>();

  get isExternalPaginator() {
    return this.externalPaginator && this.value;
  }

  get isInternalPaginator() {
    return this.internalPaginator && !this.externalPaginator && this.value;
  }

  get isLoading() {
    return this.loading && !this.value;
  }

  constructor() {
    this.columns = [];
    this.externalPaginator = false;
    this.first = 0;
    this.internalPaginator = true;
    this.loading = true;
    this.noDataSign = '- -';
    this.rowHover = false;
    this.rows = 10;
    this.rowsPerPageOptions = [10, 25, 50];
  }

  ngOnInit(): void {
  }

  getValue(row: any[], field: string) {
      return get(row, field, this.noDataSign);
  }

  onPaginate(event) {
    this.pageChange.emit(event);
  }

  onRowClick(event) {
    this.rowClick.emit(event);
  }
}
