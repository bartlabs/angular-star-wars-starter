import { Component, OnInit } from '@angular/core';
import { ThemesService } from '@app/services';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  side: string;

  get sidePersonIcon() {
    const side = this.side || 'light';
    return `assets/icons/${side}-person-icon.png`;
  }

  constructor(private _themesService: ThemesService) {
    const themeStorageKey = _themesService.themeStorageKey;

    this.side = localStorage.getItem(themeStorageKey);
  }

  ngOnInit(): void {
  }

}
