import { Component, OnInit } from '@angular/core';

import { RoutesService } from '@app/services';

@Component({
  selector: 'app-content-header',
  templateUrl: './content-header.component.html',
  styleUrls: ['./content-header.component.scss']
})
export class ContentHeaderComponent implements OnInit {
  pageTitle: string;

  constructor(
    private _routesService: RoutesService
  ) {
    this._routesService.getRoutesTitle().subscribe(title => {
      this.pageTitle = title;
    });
  }

  ngOnInit(): void { }

}
