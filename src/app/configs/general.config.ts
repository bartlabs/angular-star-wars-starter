import { GeneralConfigUrlsEnum } from '@app/enums';
import { GeneralConfigModel } from '@app/models';

export const GENERAL_CONFIG: GeneralConfigModel = {
    // env: <'dev' | 'test' | 'stage' | 'prod'> 'dev',
    // requests: {
    //     timeout: 60000
    // },
    // defaultTemplate: TemplatesEnum.Dashboard,
    urls: {
        [GeneralConfigUrlsEnum.Local]: 'http://localhost:4200/',
        [GeneralConfigUrlsEnum.SwapiApi]: 'http://localhost:3000/', // mock SwapiApi data
    },
};
