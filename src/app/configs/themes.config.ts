import {
    ThemesEnum as themes,
    ThemesPropsEnum as props
} from '../enums';
import { ThemesConfigType } from '../types';

export const THEMES_CONFIG: ThemesConfigType = {
    // First is default
    [themes.Dark]: {
        [props.ColorVeryDark]: '#222629',
        [props.ColorDark]: '#474a4f',
        [props.ColorMedium]: '#6a6e71',
        [props.ColorLight]: '#bfbfbf',
        [props.ColorVeryLight]: '#fff',
        [props.ColorAction]: '#ff0000'
    },
    [themes.Light]: {
        [props.ColorVeryDark]: '#273a63',
        [props.ColorDark]: '#637bad',
        [props.ColorMedium]: '#adb9d3',
        [props.ColorLight]: '#edf0f5',
        [props.ColorVeryLight]: '#fff',
        [props.ColorAction]: '#fff700'
    }
};
