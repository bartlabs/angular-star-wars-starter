import { ModulesEnum } from '@app/enums';
import { RoutesPathsConfigModel } from '@app/models';

export const ROUTES_PATHS_CONFIG: RoutesPathsConfigModel = {
    [ModulesEnum.Dashboard]: {
        Path: 'dashboard',
        Child: {
            Films: { Path: 'films', Name: 'Films' },
            People: { Path: 'people', Name: 'People' },
            Planets: { Path: 'planets', Name: 'Planets' },
            Species: { Path: 'species', Name: 'Species' },
            Starships: { Path: 'starships', Name: 'Starships' },
            Vehicles: { Path: 'vehicles', Name: 'Vehicles' }
        }
    }
};
