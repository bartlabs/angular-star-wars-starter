import { AssetsEnum, AssetsTypeEnum } from '@app/enums';
import { AssetsConfigType } from '@app/types';

export const ASSETS_CONFIG: AssetsConfigType = {
    [AssetsTypeEnum.Images]: {
        [AssetsEnum.DefaultAvatar]: 'default-avatar.png'
    }
};
