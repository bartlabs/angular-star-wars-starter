import { GeneralConfigUrlsEnum as url, UrlsConfigResourcesEnum as resurce } from '../enums';
import { UrlsConfigType } from '../types';

export const URLS_CONFIG: UrlsConfigType = {
    [url.SwapiApi]: {
        [resurce.Menu]: '',
        [resurce.Films]: 'films/',
        [resurce.People]: 'people/',
        [resurce.Planets]: 'planets/',
        [resurce.Species]: 'species/',
        [resurce.Starship]: 'starships/',
        [resurce.Vehicles]: 'vehicles/'
    }
};
