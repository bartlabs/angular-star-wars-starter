export class SwapiQueryParamsModel {
    page: number;
    q: string;
}
