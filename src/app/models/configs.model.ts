import { ModulesEnum } from '@app/enums';
import { GeneralConfigUrlsType } from '@app/types';

export class GeneralConfigModel {
    urls: GeneralConfigUrlsType;
}

export class RoutesPathConfigModel {
    Child?: { [key: string]: RoutesPathConfigModel };
    Name?: string;
    Path: string;
}

export class RoutesPathsConfigModel {
    [ModulesEnum.Dashboard]: RoutesPathConfigModel;
}
