import get from 'lodash/get';

import { ASSETS_CONFIG } from '@app/configs';
import { AssetsEnum, AssetsTypeEnum } from '@app/enums';

const dirName = 'assets';
const noPathResponse = null;

export function getImagePath(asset: AssetsEnum) {
    const folderName = AssetsTypeEnum.Images;
    const imagePath = get(ASSETS_CONFIG, [folderName, asset]);

    return (imagePath) ? `${dirName}/${folderName}/${imagePath}` : noPathResponse;
}
