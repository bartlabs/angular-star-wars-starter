export function joinGetParams(url: string, data: any) {
  let queryUrl = url;
  if (data) {
    const getParams = Object.entries(data).map(([key, value]) => `${key}=${value}`);
    const params = getParams.join('&');
    params && (queryUrl = `${url}?${getParams.join('&')}`);
  }

  return queryUrl;
}
