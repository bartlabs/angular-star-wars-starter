import { isArray, map, upperCase } from 'lodash';

export function prepareSwapiData(data: any[]): any[] {
  let preparedData = [];
  if (data) {
    preparedData =
      map(data, (value, key) => ({
        title: upperCase(key),
        values: isArray(value) ?
          map(value, (val) => parse(val)) : [parse(value)],
      }));
  }

  return preparedData;
}

function parse(value: any) {
  value = { value };
//  value = removeBaseUrl(value);
  value = checkDate(value);
  return value;
}

// function removeBaseUrl(value: any) {
//   const baseUrl = GENERAL_CONFIG.urls[GeneralConfigUrlsEnum.SwapiApi];
//   let _value = trim(value.value);

//   if (startsWith(_value, baseUrl)) {
//     _value = replace(_value, baseUrl, '');
//     value = { value: _value, routerLink: true };
//   }
//   return value;
// }

function checkDate(value: any) {
  const _regExp = new RegExp('^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(.[0-9]+)?(Z)?$');
  return _regExp.test(value.value) ?
    { value: new Date(value.value).toLocaleString(), date: true } : value;
}
