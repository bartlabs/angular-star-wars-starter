import { get } from 'lodash';

import { HttpHeaders } from '@angular/common/http';
import { GENERAL_CONFIG, URLS_CONFIG } from '@app/configs';
import { GeneralConfigUrlsEnum, UrlsConfigResourcesEnum } from '@app/enums';

export abstract class MainBaseService {
  private _defaultHeaders = new HttpHeaders().set('Content-Type', 'application/json; charset=UTF-8');

  public apiUrl: GeneralConfigUrlsEnum;
  public resources: UrlsConfigResourcesEnum;

  constructor() {}

  public get headers() {
      return this._defaultHeaders;
  }

  public get options() {
      return { headers: this._defaultHeaders };
  }

  public get url() {
    const baseUrl = get(GENERAL_CONFIG.urls, this.apiUrl);
    const resources = get(URLS_CONFIG, [this.apiUrl, this.resources]);

    return baseUrl + resources;
  }

  // ERROR HANDLER
  // public handleError(httpErrorResponse: HttpErrorResponse): ErrorObservable {
  // #TODO
  // }
}
