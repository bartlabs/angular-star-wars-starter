import { TestBed } from '@angular/core/testing';

import { MainBaseService } from './main-base.service';

describe('MainBaseService', () => {
  let service: MainBaseService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MainBaseService]
    });
    service = TestBed.inject(MainBaseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
