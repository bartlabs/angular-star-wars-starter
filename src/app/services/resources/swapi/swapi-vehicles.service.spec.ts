import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { SwapiVehiclesService } from './swapi-vehicles.service';

describe('SwapiVehiclesService', () => {
  let service: SwapiVehiclesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(SwapiVehiclesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
