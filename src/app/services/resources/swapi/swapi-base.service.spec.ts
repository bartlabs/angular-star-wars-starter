import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { SwapiBaseService } from './swapi-base.service';

describe('SwapiBaseService', () => {
  let service: SwapiBaseService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(SwapiBaseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
