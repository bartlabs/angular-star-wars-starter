import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UrlsConfigResourcesEnum } from '@app/enums';

import { SwapiBaseService } from './swapi-base.service';

@Injectable({
  providedIn: 'root'
})
export class SwapiSpeciesService extends SwapiBaseService {
  constructor(
    private _httpClient: HttpClient
  ) {
    super(_httpClient);

    this.resources = UrlsConfigResourcesEnum.Species;
  }
}
