import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { SwapiSpeciesService } from './swapi-species.service';

describe('SwapiSpeciesService', () => {
  let service: SwapiSpeciesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(SwapiSpeciesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
