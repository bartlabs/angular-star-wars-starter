import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { SwapiPlanetsService } from './swapi-planets.service';

describe('SwapiPlanetsService', () => {
  let service: SwapiPlanetsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(SwapiPlanetsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
