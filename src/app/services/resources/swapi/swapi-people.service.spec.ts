import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { SwapiPeopleService } from './swapi-people.service';

describe('SwapiPeopleService', () => {
  let service: SwapiPeopleService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(SwapiPeopleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
