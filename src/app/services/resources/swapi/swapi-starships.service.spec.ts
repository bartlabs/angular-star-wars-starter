import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { SwapiStarshipsService } from './swapi-starships.service';

describe('SwapiStarshipsService', () => {
  let service: SwapiStarshipsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(SwapiStarshipsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
