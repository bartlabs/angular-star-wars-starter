import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GeneralConfigUrlsEnum } from '@app/enums';
import { SwapiQueryParamsModel } from '@app/models';
import { SwapiType } from '@app/types';
import { joinGetParams } from '@app/utils';

import { MainBaseService } from '../main-base.service';

@Injectable({
  providedIn: 'root'
})
export abstract class SwapiBaseService extends MainBaseService {

  private get _url() {
    return this.url;
  }

  constructor(
    private _http: HttpClient
  ) {
    super();

    this.apiUrl = GeneralConfigUrlsEnum.SwapiApi;
  }

  getList(queryParams: SwapiQueryParamsModel = null): Observable<SwapiType[]> {
    return this._http.get<SwapiType[]>(
      joinGetParams(this._url, queryParams)
    );
  }
}
