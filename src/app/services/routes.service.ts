import { forEach, pick } from 'lodash';
import { Observable } from 'rxjs';
import { filter, map, mergeMap } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { ActivatedRoute, Data, NavigationEnd, Params, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RoutesService {
  navigationEndEvents: Observable<any>;
  queryParams: Observable<Data>;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router
  ) {
    this.navigationEndEvents = _router.events.pipe(filter(event => event instanceof NavigationEnd));
    this.queryParams = this._activatedRoute.queryParams;
  }

  getBreadcrumbTitle() {
    return this._getRoutesData().pipe(map(data => data.breadcrumb));
  }

  getRoutesTitle() {
    return this._getRoutesData().pipe(map(data => data.title));
  }

  getQueryData(expectedParams: string[] = []): string[] {
    const allQueryParams = this._activatedRoute.snapshot.queryParams;
    const queryParams = [];

    forEach(expectedParams, (paramName) => {
      allQueryParams[paramName] && (queryParams[paramName] = allQueryParams[paramName]);
    });

    return queryParams;
  }

  observableQueryData(expectedParams: string[] = []): Observable<any> {
    const queryParamsObs: Observable<Params> = this._activatedRoute.queryParams;

    expectedParams.length &&
      queryParamsObs.pipe(filter(routeParams => pick(routeParams, expectedParams)));

    return queryParamsObs;
  }

  setQueryParams(queryParams: any = {}) {
    this._router.navigate([], {
      queryParams
    });
  }

  private _getRoutesData(): Observable<Data> {
    return this.navigationEndEvents
      .pipe(map(() => {
        let route = this._activatedRoute.firstChild;
        let child = route;
        while (child) {
          if (child.firstChild) {
            child = child.firstChild;
            route = child;
          } else {
            child = null;
          }
        }
        return route;
      }),
        mergeMap(route => route.data)
      );
  }
}
