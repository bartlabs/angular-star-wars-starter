import cssVars from 'css-vars-ponyfill';
import { head, keys, toString } from 'lodash';

import { Injectable } from '@angular/core';
import { THEMES_CONFIG } from '@app/configs';
import { ThemesEnum as themes } from '@app/enums';

@Injectable({
  providedIn: 'root'
})
export class ThemesService {
  themeStorageKey: string;

  constructor() {
    this.themeStorageKey = 'Theme';
  }

  setTheme(theme: themes) {
    !THEMES_CONFIG[theme] && (theme = this.getDefaultTheme());

    const variables = THEMES_CONFIG[theme];
    localStorage.setItem(this.themeStorageKey, toString(theme));
    cssVars({ variables });
  }

  getThemeFromStorage() {
    const themeStorageValue = localStorage.getItem(this.themeStorageKey) || this.getDefaultTheme();
    themeStorageValue && themes[themeStorageValue] && this.setTheme(themes[themeStorageValue]);
  }

  private getDefaultTheme(): themes {
    // First is default
    return head(keys(themes));
  }
}
