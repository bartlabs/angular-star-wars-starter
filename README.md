# Angular STAR WARS starter

Angular 9 starter kit - no matter which side of the Power you're on ;)

### Quick start

```bash
# clone our repo
$ git clone https://bartlabs@bitbucket.org/bartlabs/angular-star-wars-starter.git <YOUR_PROJECT_NAME>

# go to your project directory
$ cd <YOUR_PROJECT_NAME>

# install the dependencies with npm
$ npm install

# in first terminal run the API server (backend)
$ npm run api

# in second terminal run the application (front)
$ npm run start
```

in your browser go to [http://localhost:4200](http://localhost:4200)

API server: [http://localhost:3000](http://localhost:3000)

---

##### 1) mockup data

In the beginning, the application was based on the remote [SWAPI](https://swapi.co) API, but it was very unstable so it currently works on mocked data: `src/mockup-data/swapi.json`.

##### 2) Theme switcher

To use the theme switcher need to add "appThemeSwitcher" directive, e.g. :

```<router-outlet appThemeSwitcher></router-outlet>```

Themes color set is defined in the `src/app/configs/themes.config.ts` file:
```
export const THEMES_CONFIG: ThemesConfigType = {
    (...)
    [themes.Light]: {
        [props.ColorVeryDark]: '#273a63',
        [props.ColorDark]: '#637bad',
        [props.ColorMedium]: '#adb9d3',
        [props.ColorLight]: '#edf0f5',
        [props.ColorVeryLight]: '#fff',
        [props.ColorAction]: '#fff700'
    }
    (...)
};
```

and used as css variables in your css/scss file, e.g.:

```
    .test-class {
        background-color: var(--colorDark);
        color: var(--colorVeryLight);
    }
```
---

### Project structure

```
angular-star-wars-starter
├── dist                        # Compiled version
├── docs                        # Documentation
├── e2e
├── node_modules
└── src
    ├── app
    |    ├── components
    |    ├── configs
    |    ├── directives
    |    ├── enums
    |    ├── interfaces
    |    ├── models
    |    ├── modules
    |    ├── services
    |    ├── types
    |    └── utils
    ├── assets                  # Images and other asset files
    ├── environments
    ├── mockup-data             # JSON files for mockup data
    └── styles                  # Global styles
```

### NPM scripts commands

`npm run ...`

| Task              | Description                                            |
|-------------------|--------------------------------------------------------|
| `start`           | Run application                                        |
| `start:ssr`       | Run Universal (SSR) application                        |
| `build`           | Full production build.                                 |
| `build:ssr`       | Build and rendering app with Universal (SSR)           |
| `api`             | Run JSON server with mockup data                       |
| `lint`            | Lint with tslint                                       |
| `test`            | Runs unit tests                                        |


## Packages

| Package               | Description                                                  |
| --------------------- | ------------------------------------------------------------ |
| bootstrap             | Sleek, intuitive, and powerful front-end framework.          |
| css-vars-ponyfill     | Provides client-side support for CSS custom properties.      |
| json-server           | Local REST API from JSON files.                              |
| lodash                | Javascript utilities library.                                |
| primeng               | Rich set of open source native Angular UI components.        |
| primeicons            | Font Icon Library for Prime UI Libraries.                    |


## Todo list

- [X] CSS framework
- [X] Server side rendering (SSR)
- [X] Progressive web app (PWA)
- [ ] NgRx
- [X] Mockup data
- [X] Local REST API
- [ ] "Clearly" styles import
- [X] Theme switcher
- [ ] Internationalization (i18n)
- [ ] Full unit tests coverage
- [ ] Full RWD
- [ ] Lodash imports optimization
- [ ] Build optimization - gzip compression
- [ ] Lazy-loading feature modules
- [ ] HttpClient manager with timeout and notifications